<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Pinger Demo repository

This repository contains an demonstration Go module implementing a Windows
service which logs `ping` at a set frequency.

This repository depends on https://gitlab.com/zygoon/go-cmdr and
https://gitlab.com/zygoon/go-winsvc

## Usage

Build the `pinger.exe` service with `go build ./cmd/pinger/`. The executable
provides a set of sub-commands for working with the service. Everything apart
from `pinger.exe debug` requires elevated permissions to talk to the Windows
service manager.

When the service is installed with `pinger.exe install` it will remain
registered in your system. Please use `pinger.exe remove` to remove it. The
service is not set to auto-start and ignores any start arguments.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.16 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
