module gitlab.com/zygoon/pinger-demo

go 1.21

require golang.org/x/sys v0.14.0

require gitlab.com/zygoon/go-cmdr v1.9.0

require gitlab.com/zygoon/go-winsvc v0.2.0
