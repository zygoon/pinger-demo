// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

//go:build windows
// +build windows

package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-winsvc"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/mgr"
)

func main() {
	cmd := winsvc.Wrapper{
		ServiceName: "pinger",
		ServiceCmd:  cmdr.Func(pinger),
		InstallConfig: mgr.Config{
			DisplayName: "Pinger demo service",
		},
	}

	cmdr.RunMain(&cmd, os.Args)
}

func pinger(ctx context.Context, args []string) error {
	ev, _ := winsvc.Env(ctx)
	if ev == nil {
		return fmt.Errorf("pinger must run as a Windows service")
	}

	const acceptMask = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue

	ev.S <- svc.Status{State: svc.StartPending}

	fastTicker := time.NewTicker(1 * time.Second)
	defer fastTicker.Stop()
	slowTicker := time.NewTicker(5 * time.Second)
	defer slowTicker.Stop()
	ticker := fastTicker

	ev.S <- svc.Status{State: svc.Running, Accepts: acceptMask}

loop:
	for n := 0; ; n++ {
		select {
		case <-ticker.C:
			ev.L.Info(1, fmt.Sprintf("ping #%d", n))

		case c := <-ev.R:
			switch c.Cmd {
			case svc.Interrogate:
				ev.S <- c.CurrentStatus

			case svc.Stop, svc.Shutdown:
				ev.L.Info(1, "Stopping or shutting down")
				break loop

			case svc.Pause:
				ev.L.Info(1, "Switching to slow tick interval")
				ev.S <- svc.Status{State: svc.Paused, Accepts: acceptMask}
				ticker = slowTicker

			case svc.Continue:
				ev.L.Info(1, "Switching to fast tick interval")
				ev.S <- svc.Status{State: svc.Running, Accepts: acceptMask}
				ticker = fastTicker

			default:
				ev.L.Error(1, fmt.Sprintf("unexpected control request #%v", c))
			}
		}
	}

	ev.S <- svc.Status{State: svc.StopPending}

	return nil
}
